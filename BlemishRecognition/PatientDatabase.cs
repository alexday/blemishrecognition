﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlemishRecognition
{
    class PatientDatabase
    {
        // Private Data Members
        private static readonly string CONNECTION_STRING = "server=localhost;user=root;database=OMSAD;port=3306;password=OMSADPassword"; // The string that addresses the database

        /// <summary>
        /// Get the database connection object
        /// </summary>
        /// <returns>MySQLConnection object connected to the omsad database</returns>
        private static MySqlConnection GetDatabseConnection()
        {
            return new MySqlConnection(CONNECTION_STRING);
        }

        public static List<Patient> GetAllPatients()
        {
            // List to return full of patients
            List<Patient> patientList = new List<Patient>();

            try
            {
                // Try and connect to the database
                using (MySqlConnection conn = GetDatabseConnection())
                {

                    // Try to open the connection
                    conn.Open();

                    // Query the database, selecting all of the patients
                    string sql = "SELECT * FROM patientinfo";
                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    // Execute the command and read the response
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        // Read to the end of the response
                        while (rdr.Read())
                        {
                            // Get the patient id
                            int patientId = (int)rdr[0];

                            // Add each patient to the list
                            patientList.Add(new Patient((int)rdr[0], rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), rdr[4].ToString(),
                                rdr[5].ToString(), rdr[6].ToString(), rdr[7].ToString(), rdr[8].ToString(), rdr[9].ToString(),
                                rdr[10].ToString()));
                        }

                    }
                }

                // Return the patient list
                return patientList;
            }

            // Catch any exception
            catch (Exception ex)
            {
                // Write the exception to the console
                Debug.WriteLine(ex.ToString());

                // Return null instead of an empty list to signify a database error
                return null;
            }
        }


        public static List<HandImage> GetLeftHandImages(int patientId)
        {
            return GetHandImages(patientId, "leftHandImages");
        }

        public static List<HandImage> GetRightHandImages(int patientId)
        {
            return GetHandImages(patientId, "rightHandImages");
        }

        private static List<HandImage> GetHandImages(int patientId, string tableName)
        {
            List<HandImage> handImages = new List<HandImage>();

            try
            {
                // Try and connect to the database
                using (MySqlConnection conn = GetDatabseConnection())
                {

                    // Try to open the connection
                    conn.Open();

                    // Query the database, selecting all of the hand images from the specified table with the specified ID
                    string sql = "SELECT id, Image FROM " + MySqlHelper.EscapeString(tableName) + " WHERE patientid = " + MySqlHelper.EscapeString(patientId.ToString());
                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    // Execute the command and read the response
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        // Read to the end of the response
                        while (rdr.Read())
                        {
                            // Construct the hand image objects
                            handImages.Add(new HandImage((int)rdr[0], ByteToImage((byte[])rdr[1])));
                        }
                    }
                }

                // Return the hand list
                return handImages;
            }

            // Catch any exception
            catch (Exception ex)
            {
                // Write the exception to the console
                Debug.WriteLine(ex.ToString());

                // Return null instead of an empty list to signify a database error
                return null;
            }
        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;

        }
    }
}
