﻿using System.Collections.Generic;

namespace BlemishRecognition
{
    public class Patient
    {
        // Public Data Members, all information from the database
        public int PatientId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Ssn { get; }
        public string Address1 { get; }
        public string Address2 { get; }
        public string City { get; }
        public string State { get; }
        public string Zip { get; }
        public string Phone { get; }
        public string Email { get; }
        public List<HandImage> RightHandImages { get; set; }
        public List<HandImage> LeftHandImages { get; set; }

        /// <summary>
        /// Construct a new Patient object
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="ssn"></param>
        /// <param name="address1"></param>
        /// <param name="address2"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zip"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        public Patient(int patientId, string firstName, string lastName, string ssn, string address1, string address2,
            string city, string state, string zip, string phone, string email)
        {
            PatientId = patientId;
            FirstName = firstName;
            LastName = lastName;
            Ssn = ssn;
            Address1 = address1;
            Address2 = address2;
            City = city;
            State = state;
            Zip = zip;
            Phone = phone;
            Email = email;
            RightHandImages = new List<HandImage>();
            LeftHandImages = new List<HandImage>();
        }

        /// <summary>
        /// Retrieve hand images from the database
        /// </summary>
        /// <returns>True if success</returns>
        public bool RetrieveHandImages()
        {
            // Get the left and right hand images from the database
            RightHandImages = PatientDatabase.GetRightHandImages(PatientId);
            LeftHandImages = PatientDatabase.GetLeftHandImages(PatientId);

            // If either of them are null then a database error occured so return false
            return (RightHandImages != null) && (LeftHandImages != null);
        }
        
        /// <summary>
        /// Overridden Patient ToString method
        /// </summary>
        /// <returns>Last Name, First Name</returns>
        public override string ToString()
        {
            return LastName + ", " + FirstName;
        }
    }
}