﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlemishRecognition
{
    public partial class HandImageForm : Form
    {
        private HandImage handImage;
        private List<Blemish> blemishes;
        private Blemish currentBlemish;
        private bool mouseDown;
        private int initialHeight;
        private int initialWidth;

        public HandImageForm(HandImage handImage)
        {
            InitializeComponent();

            blemishes = new List<Blemish>();
            currentBlemish = new Blemish();

            this.handImage = handImage;
        }

        private void HandImageForm_Load(object sender, EventArgs e)
        {
            initialHeight = picHandImage.Height;
            initialWidth = picHandImage.Width;
        }

        private void pnlHandImage_Paint(object sender, PaintEventArgs e)
        {
            DrawHandImage(e.Graphics);
            DrawCurrentBlemish(e.Graphics);
            DrawStoredBlemishes(e.Graphics);
        }

        private void DrawStoredBlemishes(Graphics graphics)
        {
            float widthScale = (float)picHandImage.Width / (float)handImage.Image.Width;
            float heightScale = (float)picHandImage.Height / (float)handImage.Image.Height;

            foreach (Blemish blemish in blemishes)
            {


                graphics.ScaleTransform(widthScale, heightScale);

                if (blemish.Angle != 0)
                {
                    graphics.TranslateTransform(blemish.Position.X + (blemish.Position.Size.Width / 2),
                                            blemish.Position.Y + (blemish.Position.Size.Height / 2));
                    graphics.RotateTransform(blemish.Angle);

                    graphics.DrawRectangle(Pens.CadetBlue, new Rectangle(-(blemish.Position.Size.Width / 2), -(blemish.Position.Size.Height / 2),
                                                                        blemish.Position.Width, blemish.Position.Height));
                }
                else
                {
                    graphics.DrawRectangle(Pens.CadetBlue, blemish.Position);
                }


                graphics.ResetTransform();
            }
        }

        private void DrawCurrentBlemish(Graphics graphics)
        {
            if (currentBlemish == null)
            {
                return;
            }

            float widthScale = (float) picHandImage.Width / (float) handImage.Image.Width;
            float heightScale = (float) picHandImage.Height / (float) handImage.Image.Height;

            graphics.ScaleTransform(widthScale, heightScale);

            if(currentBlemish.Angle != 0)
            {
                graphics.TranslateTransform(currentBlemish.Position.X + (currentBlemish.Position.Size.Width / 2),
                                        currentBlemish.Position.Y + (currentBlemish.Position.Size.Height / 2));
                graphics.RotateTransform(currentBlemish.Angle);

                graphics.DrawRectangle(Pens.AliceBlue, new Rectangle(-(currentBlemish.Position.Size.Width / 2), -(currentBlemish.Position.Size.Height / 2),
                                                                    currentBlemish.Position.Width, currentBlemish.Position.Height));
            } else
            {
                graphics.DrawRectangle(Pens.AliceBlue, currentBlemish.Position);
            }
            

            graphics.ResetTransform();
        }

        private void DrawHandImage(Graphics g)
        {
            double scalingFactor = Math.Min(((double)ClientRectangle.Width) / ((double) handImage.Image.Width), 
                ((double)ClientRectangle.Height) / ((double) handImage.Image.Height));

            int width = (int) Math.Round(handImage.Image.Width * scalingFactor);
            int height = (int) Math.Round(handImage.Image.Height * scalingFactor);

            picHandImage.Location = new Point((ClientRectangle.Width / 2) - (width / 2), (ClientRectangle.Height / 2) - (height / 2));
            picHandImage.Size = new Size(width, height);


            g.DrawImage(handImage.Image, 0, 0, width, height);
           
           
        }

        private void pnlHandImage_Resize(object sender, EventArgs e)
        {
            picHandImage.Refresh();
        }

        private void pnlHandImage_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            currentBlemish = new Blemish();
            currentBlemish.Position = new Rectangle((int) Math.Round(e.X / (double) picHandImage.Width * handImage.Image.Width),
                                                    (int) Math.Round(e.Y / (double) picHandImage.Height * handImage.Image.Height), 0, 0);
        }

        private void pnlHandImage_MouseMove(object sender, MouseEventArgs e)
        {
            if(!mouseDown)
            {
                return;
            }

            Rectangle oldPosition = currentBlemish.Position;
            currentBlemish.Position = new Rectangle(oldPosition.X, oldPosition.Y,
                ((int) Math.Round(e.X / (double) picHandImage.Width * handImage.Image.Width)) - oldPosition.X,
                ((int) Math.Round(e.Y / (double)picHandImage.Height * handImage.Image.Height)) - oldPosition.Y);

            picHandImage.Refresh();
        }

        private void pnlHandImage_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void picHandImage_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
           // MessageBox.Show("LEFT");

        }

        private void HandImageForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (currentBlemish == null)
            {
                return;
            }

            switch(e.KeyCode)
            {
                case Keys.Left:
                    if (e.Control)
                    {
                        currentBlemish.Angle -= 10;
                    } else
                    {
                        Rectangle copyPosL = currentBlemish.Position;
                        copyPosL.Offset(-10, 0);
                        currentBlemish.Position = copyPosL;
                    }
                    break;
                case Keys.Right:
                    if (e.Control)
                    {
                        currentBlemish.Angle += 10;
                    }
                    else
                    {
                        Rectangle copyPosR = currentBlemish.Position;
                        copyPosR.Offset(10, 0);
                        currentBlemish.Position = copyPosR;
                    }
                    break;
                case Keys.Up:
                    Rectangle copyPosU = currentBlemish.Position;
                    copyPosU.Offset(0, -10);
                    currentBlemish.Position = copyPosU;
                    break;
                case Keys.Down:
                    Rectangle copyPosD = currentBlemish.Position;
                    copyPosD.Offset(0, 10);
                    currentBlemish.Position = copyPosD;
                    break;
                case Keys.Enter:
                    blemishes.Add(currentBlemish);
                    currentBlemish = null;
                    break;
            }

            picHandImage.Invalidate();
        }
    }
}
