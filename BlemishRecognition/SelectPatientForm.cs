﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlemishRecognition
{
    public partial class SelectPatientForm : Form
    {
        // Private Data Members
        private List<Patient> patients; // Store the list of all the patients from the database

        /// <summary>
        /// Construct a SelectPatientForm object
        /// </summary>
        public SelectPatientForm()
        {
            // Get all of the patients from the database
            patients = PatientDatabase.GetAllPatients();

            // Initialize all of the components on the form
            InitializeComponent();
        }

        /// <summary>
        /// Load all of the patients onto the list box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPatientForm_Load(object sender, EventArgs e)
        {
            // Check if the patient list is null, if so a database error occured
            if (patients == null)
            {
                MessageBox.Show("Therer was an error connecting to the database. Please try again or contact your system administrator");
            }
            else
            {
                // Add all of the patients onto the listbox so long as it's not null
                LstAvaliablePatients.Items.AddRange(patients.ToArray());
            }

            //TODO Remove for non-debug
            LstAvaliablePatients.SetSelected(0, true);
            BtnOpenPatient.PerformClick();
        }

        /// <summary>
        /// Update the patient details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstAvaliablePatients_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Check if the selected item is null (I don't know if this is an accurate edge case
            // but it can't hurt to check)
            if (LstAvaliablePatients.SelectedItem != null)
            {
                // Set the detials pane to the currently selected patient
                Patient selectedPatient = ((Patient)LstAvaliablePatients.SelectedItem);
                TxtFirstName.Text = selectedPatient.FirstName;
                TxtLastName.Text = selectedPatient.LastName;
                TxtSSN.Text = selectedPatient.Ssn;
                TxtAddress1.Text = selectedPatient.Address1;
                TxtAddress2.Text = selectedPatient.Address2;
                TxtCity.Text = selectedPatient.City;
                TxtState.Text = selectedPatient.State;
                TxtZip.Text = selectedPatient.Zip;
                TxtPhone.Text = selectedPatient.Phone;
                TxtEmail.Text = selectedPatient.Email;
            }
        }

        /// <summary>
        /// Close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            // Close the form (and the program)
            this.Close();
        }

        private void BtnOpenPatient_Click(object sender, EventArgs e)
        {
            // If there is no patient selected early return
            if (LstAvaliablePatients.SelectedItem == null)
            {
                MessageBox.Show("Select a patient in the list before opening the patient TODO do this a better error. Maybe localization IDK ask jody.");
                return;
            }

            // Selected patient needs to get the hand images from the database because
            // it would lag too much on startup if all of them got them at the beginning
            ((Patient)LstAvaliablePatients.SelectedItem).RetrieveHandImages();

            // Create a new HandImageForm with the patient object passed in
            SelectHandImageForm selectHandImageForm = new SelectHandImageForm((Patient)LstAvaliablePatients.SelectedItem);

            // Hide this window until the user closes out of the selecthandimageform
            Hide();
            selectHandImageForm.ShowDialog();
            Show();
        }
    }
}
