﻿using System.Drawing;

namespace BlemishRecognition
{
    public class Blemish
    {
        public Rectangle Position { get; set; }
        public float Angle { get; set; }

        public Blemish()
        {
            Position = new Rectangle(0, 0, 0, 0);
            Angle = 0;
        }
    }
}