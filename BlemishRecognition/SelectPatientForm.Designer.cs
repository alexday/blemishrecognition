﻿namespace BlemishRecognition
{
    partial class SelectPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstAvaliablePatients = new System.Windows.Forms.ListBox();
            this.BtnOpenPatient = new System.Windows.Forms.Button();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.LblEmail = new System.Windows.Forms.Label();
            this.TxtPhone = new System.Windows.Forms.TextBox();
            this.LblPhone = new System.Windows.Forms.Label();
            this.TxtZip = new System.Windows.Forms.TextBox();
            this.LblZipCode = new System.Windows.Forms.Label();
            this.TxtState = new System.Windows.Forms.TextBox();
            this.LblState = new System.Windows.Forms.Label();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.LblCity = new System.Windows.Forms.Label();
            this.TxtAddress2 = new System.Windows.Forms.TextBox();
            this.LblAddress2 = new System.Windows.Forms.Label();
            this.TxtAddress1 = new System.Windows.Forms.TextBox();
            this.LblAddress1 = new System.Windows.Forms.Label();
            this.TxtSSN = new System.Windows.Forms.TextBox();
            this.LblSSN = new System.Windows.Forms.Label();
            this.TxtLastName = new System.Windows.Forms.TextBox();
            this.LblLastName = new System.Windows.Forms.Label();
            this.TxtFirstName = new System.Windows.Forms.TextBox();
            this.LblPatientFirstName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LstAvaliablePatients
            // 
            this.LstAvaliablePatients.FormattingEnabled = true;
            this.LstAvaliablePatients.ItemHeight = 16;
            this.LstAvaliablePatients.Location = new System.Drawing.Point(12, 12);
            this.LstAvaliablePatients.Name = "LstAvaliablePatients";
            this.LstAvaliablePatients.Size = new System.Drawing.Size(306, 308);
            this.LstAvaliablePatients.TabIndex = 1;
            this.LstAvaliablePatients.SelectedIndexChanged += new System.EventHandler(this.LstAvaliablePatients_SelectedIndexChanged);
            // 
            // BtnOpenPatient
            // 
            this.BtnOpenPatient.Location = new System.Drawing.Point(575, 294);
            this.BtnOpenPatient.Name = "BtnOpenPatient";
            this.BtnOpenPatient.Size = new System.Drawing.Size(127, 26);
            this.BtnOpenPatient.TabIndex = 2;
            this.BtnOpenPatient.Text = "Open Patient...";
            this.BtnOpenPatient.UseVisualStyleBackColor = true;
            this.BtnOpenPatient.Click += new System.EventHandler(this.BtnOpenPatient_Click);
            // 
            // TxtEmail
            // 
            this.TxtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtEmail.Location = new System.Drawing.Point(430, 264);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(272, 22);
            this.TxtEmail.TabIndex = 41;
            // 
            // LblEmail
            // 
            this.LblEmail.AutoSize = true;
            this.LblEmail.Location = new System.Drawing.Point(330, 267);
            this.LblEmail.Name = "LblEmail";
            this.LblEmail.Size = new System.Drawing.Size(46, 17);
            this.LblEmail.TabIndex = 40;
            this.LblEmail.Text = "Email:";
            // 
            // TxtPhone
            // 
            this.TxtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPhone.Location = new System.Drawing.Point(430, 236);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Size = new System.Drawing.Size(272, 22);
            this.TxtPhone.TabIndex = 39;
            // 
            // LblPhone
            // 
            this.LblPhone.AutoSize = true;
            this.LblPhone.Location = new System.Drawing.Point(330, 239);
            this.LblPhone.Name = "LblPhone";
            this.LblPhone.Size = new System.Drawing.Size(49, 17);
            this.LblPhone.TabIndex = 38;
            this.LblPhone.Text = "Phone";
            // 
            // TxtZip
            // 
            this.TxtZip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtZip.Location = new System.Drawing.Point(430, 208);
            this.TxtZip.Name = "TxtZip";
            this.TxtZip.Size = new System.Drawing.Size(272, 22);
            this.TxtZip.TabIndex = 37;
            // 
            // LblZipCode
            // 
            this.LblZipCode.AutoSize = true;
            this.LblZipCode.Location = new System.Drawing.Point(330, 211);
            this.LblZipCode.Name = "LblZipCode";
            this.LblZipCode.Size = new System.Drawing.Size(73, 17);
            this.LblZipCode.TabIndex = 36;
            this.LblZipCode.Text = "Zip Code: ";
            // 
            // TxtState
            // 
            this.TxtState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtState.Location = new System.Drawing.Point(430, 180);
            this.TxtState.Name = "TxtState";
            this.TxtState.Size = new System.Drawing.Size(272, 22);
            this.TxtState.TabIndex = 35;
            // 
            // LblState
            // 
            this.LblState.AutoSize = true;
            this.LblState.Location = new System.Drawing.Point(330, 183);
            this.LblState.Name = "LblState";
            this.LblState.Size = new System.Drawing.Size(41, 17);
            this.LblState.TabIndex = 34;
            this.LblState.Text = "State";
            // 
            // TxtCity
            // 
            this.TxtCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCity.Location = new System.Drawing.Point(430, 152);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(272, 22);
            this.TxtCity.TabIndex = 33;
            // 
            // LblCity
            // 
            this.LblCity.AutoSize = true;
            this.LblCity.Location = new System.Drawing.Point(330, 155);
            this.LblCity.Name = "LblCity";
            this.LblCity.Size = new System.Drawing.Size(35, 17);
            this.LblCity.TabIndex = 32;
            this.LblCity.Text = "City:";
            // 
            // TxtAddress2
            // 
            this.TxtAddress2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtAddress2.Location = new System.Drawing.Point(430, 124);
            this.TxtAddress2.Name = "TxtAddress2";
            this.TxtAddress2.Size = new System.Drawing.Size(272, 22);
            this.TxtAddress2.TabIndex = 31;
            // 
            // LblAddress2
            // 
            this.LblAddress2.AutoSize = true;
            this.LblAddress2.Location = new System.Drawing.Point(330, 127);
            this.LblAddress2.Name = "LblAddress2";
            this.LblAddress2.Size = new System.Drawing.Size(76, 17);
            this.LblAddress2.TabIndex = 30;
            this.LblAddress2.Text = "Address 2:";
            // 
            // TxtAddress1
            // 
            this.TxtAddress1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtAddress1.Location = new System.Drawing.Point(430, 96);
            this.TxtAddress1.Name = "TxtAddress1";
            this.TxtAddress1.Size = new System.Drawing.Size(272, 22);
            this.TxtAddress1.TabIndex = 29;
            // 
            // LblAddress1
            // 
            this.LblAddress1.AutoSize = true;
            this.LblAddress1.Location = new System.Drawing.Point(330, 99);
            this.LblAddress1.Name = "LblAddress1";
            this.LblAddress1.Size = new System.Drawing.Size(76, 17);
            this.LblAddress1.TabIndex = 28;
            this.LblAddress1.Text = "Address 1:";
            // 
            // TxtSSN
            // 
            this.TxtSSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtSSN.Location = new System.Drawing.Point(430, 68);
            this.TxtSSN.Name = "TxtSSN";
            this.TxtSSN.Size = new System.Drawing.Size(272, 22);
            this.TxtSSN.TabIndex = 27;
            // 
            // LblSSN
            // 
            this.LblSSN.AutoSize = true;
            this.LblSSN.Location = new System.Drawing.Point(330, 71);
            this.LblSSN.Name = "LblSSN";
            this.LblSSN.Size = new System.Drawing.Size(44, 17);
            this.LblSSN.TabIndex = 26;
            this.LblSSN.Text = "SSN: ";
            // 
            // TxtLastName
            // 
            this.TxtLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtLastName.Location = new System.Drawing.Point(430, 40);
            this.TxtLastName.Name = "TxtLastName";
            this.TxtLastName.Size = new System.Drawing.Size(272, 22);
            this.TxtLastName.TabIndex = 25;
            // 
            // LblLastName
            // 
            this.LblLastName.AutoSize = true;
            this.LblLastName.Location = new System.Drawing.Point(330, 43);
            this.LblLastName.Name = "LblLastName";
            this.LblLastName.Size = new System.Drawing.Size(84, 17);
            this.LblLastName.TabIndex = 24;
            this.LblLastName.Text = "Last Name: ";
            // 
            // TxtFirstName
            // 
            this.TxtFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFirstName.Location = new System.Drawing.Point(430, 12);
            this.TxtFirstName.Name = "TxtFirstName";
            this.TxtFirstName.Size = new System.Drawing.Size(272, 22);
            this.TxtFirstName.TabIndex = 23;
            // 
            // LblPatientFirstName
            // 
            this.LblPatientFirstName.AutoSize = true;
            this.LblPatientFirstName.Location = new System.Drawing.Point(330, 15);
            this.LblPatientFirstName.Name = "LblPatientFirstName";
            this.LblPatientFirstName.Size = new System.Drawing.Size(84, 17);
            this.LblPatientFirstName.TabIndex = 22;
            this.LblPatientFirstName.Text = "First Name: ";
            // 
            // SelectPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 329);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.LblEmail);
            this.Controls.Add(this.TxtPhone);
            this.Controls.Add(this.LblPhone);
            this.Controls.Add(this.TxtZip);
            this.Controls.Add(this.LblZipCode);
            this.Controls.Add(this.TxtState);
            this.Controls.Add(this.LblState);
            this.Controls.Add(this.TxtCity);
            this.Controls.Add(this.LblCity);
            this.Controls.Add(this.TxtAddress2);
            this.Controls.Add(this.LblAddress2);
            this.Controls.Add(this.TxtAddress1);
            this.Controls.Add(this.LblAddress1);
            this.Controls.Add(this.TxtSSN);
            this.Controls.Add(this.LblSSN);
            this.Controls.Add(this.TxtLastName);
            this.Controls.Add(this.LblLastName);
            this.Controls.Add(this.TxtFirstName);
            this.Controls.Add(this.LblPatientFirstName);
            this.Controls.Add(this.BtnOpenPatient);
            this.Controls.Add(this.LstAvaliablePatients);
            this.Name = "SelectPatientForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.SelectPatientForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox LstAvaliablePatients;
        private System.Windows.Forms.Button BtnOpenPatient;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.Label LblEmail;
        private System.Windows.Forms.TextBox TxtPhone;
        private System.Windows.Forms.Label LblPhone;
        private System.Windows.Forms.TextBox TxtZip;
        private System.Windows.Forms.Label LblZipCode;
        private System.Windows.Forms.TextBox TxtState;
        private System.Windows.Forms.Label LblState;
        private System.Windows.Forms.TextBox TxtCity;
        private System.Windows.Forms.Label LblCity;
        private System.Windows.Forms.TextBox TxtAddress2;
        private System.Windows.Forms.Label LblAddress2;
        private System.Windows.Forms.TextBox TxtAddress1;
        private System.Windows.Forms.Label LblAddress1;
        private System.Windows.Forms.TextBox TxtSSN;
        private System.Windows.Forms.Label LblSSN;
        private System.Windows.Forms.TextBox TxtLastName;
        private System.Windows.Forms.Label LblLastName;
        private System.Windows.Forms.TextBox TxtFirstName;
        private System.Windows.Forms.Label LblPatientFirstName;
    }
}

