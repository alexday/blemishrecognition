﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlemishRecognition
{
    /// <summary>
    /// Image of a singular hand with blemishes
    /// </summary>
    public class HandImage
    {
        // Private Data Members
        public int ID { get; }                  // Store the ID of the image in the database
        public Image Image { get; }             // Store the image of the hand
        public List<Blemish> Blemishes { get; } // Store all of the blemishes on the hand

        /// <summary>
        /// Construct a Hand Image object
        /// </summary>
        /// <param name="Id">ID of the image in the database</param>
        /// <param name="image">Image of the hand</param>
        public HandImage(int Id, Image image)
        {
            ID = Id;
            Image = image;
            Blemishes = new List<Blemish>();
        }
    }
}
