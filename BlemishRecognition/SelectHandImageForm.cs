﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlemishRecognition
{
    public partial class SelectHandImageForm : Form
    {
        private Patient patient;

        public SelectHandImageForm(Patient patient)
        {
            this.patient = patient;
            InitializeComponent();
        }

        private void SelectHandImageForm_Load(object sender, EventArgs e)
        {
            // Set the list views images and names for selecion
            SetListViewHandImages(lstViewLeftHandImages, patient.LeftHandImages);
            SetListViewHandImages(lstViewRightHandImages, patient.RightHandImages);
        }

        private void SetListViewHandImages(ListView lstView, List<HandImage> handImages)
        {
            // Create a new "Image Dictionary" and fill it with the image and then the "key"
            var imageList = new ImageList();
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            int imageAddKey = 0;

            foreach(HandImage handImage in handImages)
            {
                imageList.Images.Add((imageAddKey++).ToString(), handImage.Image);
            }
            // Set the lsit views image list
            lstView.LargeImageList = imageList;

            imageAddKey = 0;
            foreach(HandImage handImage in handImages)
            {
                ListViewItem listViewItem = lstView.Items.Add(handImage.ID.ToString());
                listViewItem.ImageKey = (imageAddKey++).ToString();
            }
        }

        private void BtnOpenSelectedImage_Click(object sender, EventArgs e)
        {
            // Call the open selected hand on both list views because only one will have one selected
            OpenHandImageFromList(lstViewLeftHandImages, patient.LeftHandImages);
            OpenHandImageFromList(lstViewRightHandImages, patient.RightHandImages);
        }

        private void OpenHandImageFromList(ListView lstView, List<HandImage> handImages)
        {
            // If there are no selected images return
            if(lstView.SelectedItems.Count == 0)
            {
                return;
            }

            // Else pass the selected image into the hand image form and show the form
            HandImageForm handImageForm = new HandImageForm(handImages[lstView.SelectedIndices[0]]);

            Hide();
            handImageForm.ShowDialog();
            Show();
        }

        private void lstViewRightHandImages_DoubleClick(object sender, EventArgs e)
        {
           
        }
    }
}
