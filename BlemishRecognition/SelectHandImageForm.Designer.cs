﻿namespace BlemishRecognition
{
    partial class SelectHandImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstViewLeftHandImages = new System.Windows.Forms.ListView();
            this.lstViewRightHandImages = new System.Windows.Forms.ListView();
            this.BtnOpenSelectedImage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstViewLeftHandImages
            // 
            this.lstViewLeftHandImages.Location = new System.Drawing.Point(12, 12);
            this.lstViewLeftHandImages.Name = "lstViewLeftHandImages";
            this.lstViewLeftHandImages.Size = new System.Drawing.Size(281, 365);
            this.lstViewLeftHandImages.TabIndex = 0;
            this.lstViewLeftHandImages.UseCompatibleStateImageBehavior = false;
            // 
            // lstViewRightHandImages
            // 
            this.lstViewRightHandImages.Location = new System.Drawing.Point(300, 12);
            this.lstViewRightHandImages.Name = "lstViewRightHandImages";
            this.lstViewRightHandImages.Size = new System.Drawing.Size(281, 365);
            this.lstViewRightHandImages.TabIndex = 1;
            this.lstViewRightHandImages.UseCompatibleStateImageBehavior = false;
            this.lstViewRightHandImages.DoubleClick += new System.EventHandler(this.lstViewRightHandImages_DoubleClick);
            // 
            // BtnOpenSelectedImage
            // 
            this.BtnOpenSelectedImage.Location = new System.Drawing.Point(366, 383);
            this.BtnOpenSelectedImage.Name = "BtnOpenSelectedImage";
            this.BtnOpenSelectedImage.Size = new System.Drawing.Size(215, 33);
            this.BtnOpenSelectedImage.TabIndex = 2;
            this.BtnOpenSelectedImage.Text = "Open Selected Hand Image...";
            this.BtnOpenSelectedImage.UseVisualStyleBackColor = true;
            this.BtnOpenSelectedImage.Click += new System.EventHandler(this.BtnOpenSelectedImage_Click);
            // 
            // SelectHandImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 428);
            this.Controls.Add(this.BtnOpenSelectedImage);
            this.Controls.Add(this.lstViewRightHandImages);
            this.Controls.Add(this.lstViewLeftHandImages);
            this.Name = "SelectHandImageForm";
            this.Text = "Select Hand Image";
            this.Load += new System.EventHandler(this.SelectHandImageForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstViewLeftHandImages;
        private System.Windows.Forms.ListView lstViewRightHandImages;
        private System.Windows.Forms.Button BtnOpenSelectedImage;
    }
}