﻿namespace BlemishRecognition
{
    partial class HandImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.PictureBox picHandImage;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picHandImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picHandImage)).BeginInit();
            this.SuspendLayout();
            // 
            // picHandImage
            // 
            this.picHandImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picHandImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picHandImage.Location = new System.Drawing.Point(12, 12);
            this.picHandImage.Name = "picHandImage";
            this.picHandImage.Size = new System.Drawing.Size(776, 426);
            this.picHandImage.TabIndex = 0;
            this.picHandImage.TabStop = false;
            this.picHandImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlHandImage_Paint);
            this.picHandImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlHandImage_MouseDown);
            this.picHandImage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlHandImage_MouseMove);
            this.picHandImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlHandImage_MouseUp);
            this.picHandImage.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.picHandImage_PreviewKeyDown);
            this.picHandImage.Resize += new System.EventHandler(this.pnlHandImage_Resize);
            // 
            // HandImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.picHandImage);
            this.Name = "HandImageForm";
            this.Text = "HandImageForm";
            this.Load += new System.EventHandler(this.HandImageForm_Load);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.HandImageForm_PreviewKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.picHandImage)).EndInit();
            this.ResumeLayout(false);

        }

    }
}

#endregion
